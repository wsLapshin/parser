# create database santeh default charset cp1251 collate cp1251_general_ci;
# делай в utf8 вероятно 
create table proxy(
    id int(11) not null auto_increment primary key,
    host varchar(15) not null,
    port smallint(5) unsigned not null default 8080,
    login varchar(255) not null default '',
    password varchar(255) not null default '',
    last_visited timestamp default current_timestamp,
    active tinyint(1) not null default 1,
    attempts tinyint(3) not null default 0
) engine=MyIsam default charset utf8;

create table log4php_log (
    id int(10) not null auto_increment primary key,
    timestamp timestamp not null default current_timestamp,
    level varchar(32) not null default '',
    logger varchar(256) not null default '',
    classify varchar(100) not null default '',
    message varchar(4000) not null default '',
    thread int(11) not null default 0,
    `file` varchar(256) not null default '',
     line varchar(10) not null default '',
    addition1 varchar(2000) not null default '',
    addition2 varchar(2000) not null default '',
    addition3 varchar(2000) not null default ''
) engine=MyIsam default charset utf8;

create table categories (
    id int(10) not null auto_increment primary key,
    sys_label varchar(255) not null default '',
    url varchar(255) not null default '',
    label varchar(255) not null default ''
) engine=MyIsam default charset utf8 collate utf8_general_ci; 
--
alter table categories change url last_url varchar(255) not null default ''; 

create table products (
    id int(10) not null auto_increment primary key,
    category_id int(10) not null,
    url varchar(255) not null default '',
    label varchar(255) not null default '',
    external_id varchar(255) not null default ''
) engine=MyIsam default charset utf8 collate utf8_general_ci; 
--
alter table products add column category_page int(11) not null default 0;

create table products_mebel_d_vannoi (
    id int(10) not null auto_increment primary key,
    product_id int(10) not null,
    seriya varchar(255) not null default '',
    model varchar(255) not null default '',
    proizvodstvo varchar(255) not null default '',
    country varchar (255) not null default '',
    price varchar(255) not null default '',
    garantiya varchar(255) not null default '',
    design_style varchar(255) not null default '',
    `type` varchar(255) not null default '',
--
    width varchar(255) not null default '',
    height varchar(255) not null default '',
    glubina varchar(255) not null default '',
--
    color varchar(255) not null default '',
    color_rakovina varchar(255) not null default '',
    material_korpusa varchar(255) not null default '',
    material_fasada varchar(255) not null default '',
    material_rakovini varchar(255) not null default '',
    pokritie_korpusa varchar(255) not null default '',
    pokritie_fasada varchar(255) not null default '',
--
    assimetry varchar(255) not null default '',
    dopinfo varchar(255) not null default '',
    forma_meb varchar(255) not null default '',
    forma_rakovini varchar(255) not null default '',
    furniture varchar(255) not null default '',
    hranenie_sys varchar(255) not null default '',
    montazh varchar (255) not null default '',
    moschnost_lampi varchar(255) not null default '',
    osnaschenie varchar(255) not null default '',
    tip_lampi varchar (255) not null default '',
    tumba varchar(255) not null default '',
    uglovaya varchar(255) not null default '',
    usefor varchar(255) not null default ''
) engine=MyIsam default charset utf8 collate utf8_general_ci; 

create table products_unitazi (
    id int(10) not null auto_increment primary key,
    product_id int(10) not null,
    seriya varchar(255) not null default '',
    model varchar(255) not null default '',
    proizvodstvo varchar(255) not null default '',
    country varchar (255) not null default '',
    price varchar(255) not null default '',
    garantiya varchar(255) not null default '',
    design_style varchar(255) not null default '',
    `type` varchar(255) not null default '', 
    type_unitaz varchar(255) not null default '',
--
    width varchar(255) not null default '',
    height varchar(255) not null default '',
    `length` varchar(255) not null default '',
--
    color varchar(255) not null default '',
    color_sidenie varchar(255) not null default '',
    material varchar(255) not null default '',
    material_sidenie varchar(255) not null default '',
--
    bezobodka varchar(255) not null default '',
    bide varchar(255) not null default '',
    bistrosem_sidenie varchar(255) not null default '',
    forma varchar(255) not null default '',
    furniture varchar(255) not null default '',
    height_chasha varchar(255) not null default '',
    mehanizm_sliva varchar(255) not null default '',
    metod_ustanovki_bochka varchar(255) not null default '',
    napravlenie_vipuska varchar(255) not null default '',
    oblast_primeneniya varchar(255) not null default '',
    organizaciya_smiv_potoka varchar(255) not null default '',
    polochka_v_chashe varchar(255) not null default '',
    rezhim_sliva_vodi varchar(255) not null default '',
    sidenie_v_komplekte varchar(255) not null default '',
    sistema_antivsplesk varchar(255) not null default '',
    trebuetsya_installation varchar(255) not null default '',
    uglovaya varchar(255) not null default '',
    unitaz_mikrolift_ahuet varchar(255) not null default ''
) engine=MyIsam default charset utf8 collate utf8_general_ci; 
alter table products_unitazi add column external_id varchar(255) not null default '';
alter table products_unitazi add column article_not_external_id varchar(255) not null default '';
alter table products_unitazi add column obem_bachka varchar(255) not null default '';
alter table products_unitazi add column podvod_vodi varchar(255) not null default '';
alter table products_unitazi add column dopolnitelnie_funkcii varchar(255) not null default '';
alter table products_unitazi add column dopinfo varchar(255) not null default '';
alter table products_unitazi add column osnaschenie varchar(255) not null default '';
alter table products_unitazi add column image_description varchar(255) not null default '';
alter table products_unitazi add column sidenie_s_podogrevom varchar(255) not null default '';
alter table products_unitazi add column elektropitanie varchar(255) not null default '';
alter table products_unitazi drop column type_unitaz;


create table products_rakovini (
    id int(10) not null auto_increment primary key,
    product_id int(10) not null,
    external_id varchar(255) not null default '',
    article_not_external_id varchar(255) not null default '',
    seriya varchar(255) not null default '',
    model varchar(255) not null default '',
    proizvodstvo varchar(255) not null default '',
    country varchar (255) not null default '',
    price varchar(255) not null default '',
    garantiya varchar(255) not null default '',
    design_style varchar(255) not null default '',
    `type` varchar(255) not null default '', 
    image_description varchar(255) not null default '',
    dopinfo varchar(255) not null default '',
    oblast_primeneniya varchar(255) not null default '',
    forma varchar(255) not null default '',
--
    width varchar(255) not null default '',
    height varchar(255) not null default '',
    glubina varchar(255) not null default '',
--
    color varchar(255) not null default '',
    color_sidenie varchar(255) not null default '',
    material varchar(255) not null default '',
    assimetry varchar(255) not null default '',
    uglovaya varchar(255) not null default '',
    montazh varchar (255) not null default '',
--
    gotovie_otverstiya varchar(255) not null default '',
    namechennie_otverstiya varchar(255) not null default '',
    otverstiya_pod_smesitel varchar(255) not null default '',
    raspolojenie_smesitelya varchar(255) not null default '',
    diametr_sliva varchar(255) not null default '',
    pereliv varchar(255) not null default '',
    ustanovka_nad_stir varchar(255) not null default '',
    krishka_pereliva varchar(255) not null default '',
    furniture varchar(255) not null default ''
) engine=MyIsam default charset utf8 collate utf8_general_ci; 
alter table products_rakovini add column osnaschenie varchar(255) not null default '';
alter table products_rakovini add column orientation varchar(255) not null default '';
alter table products_rakovini add column dopolnitelnie_funkcii varchar(1000) not null default '';

create table products_smesiteli (
    id int(10) not null auto_increment primary key,
    product_id int(10) not null,
    external_id varchar(255) not null default '',
    article_not_external_id varchar(255) not null default '',
    seriya varchar(255) not null default '',
    model varchar(255) not null default '',
    proizvodstvo varchar(255) not null default '',
    country varchar (255) not null default '',
    price varchar(255) not null default '',
    garantiya varchar(255) not null default '',
    design_style varchar(255) not null default '',
    `type` varchar(255) not null default '', 
    image_description varchar(255) not null default '',
    dopinfo varchar(255) not null default '',
    forma varchar(255) not null default '',
--
    width varchar(255) not null default '',
    height varchar(255) not null default '',
    glubina varchar(255) not null default '',
    rashod_vodi varchar (255) not null default '',
--
    color varchar(255) not null default '',
    material varchar(255) not null default '',
    montazh varchar (255) not null default '',
--
    oblast_primeneniya varchar(255) not null default '',
    naznachenie varchar(255) not null default '',
    vstraivaemaya_sistema varchar(255) not null default '',
    vnutrennyaya_chast_v_komplekte varchar (255) not null default '',
    upravlenie varchar (255) not null default '',
    poverhnost varchar (255) not null default '',
    visota_izliva varchar (255) not null default '',  
    dlina_izliva  varchar (255) not null default '',
    dlina_shlanga  varchar (255) not null default '',     
    forma_izliva  varchar (255) not null default '',
    mehanizm  varchar (255) not null default '', 
    tip_podvodki  varchar (255) not null default '',
    raspolozhenie_richaga varchar (255) not null default '',
    otverstiya_dlya_montazha  varchar (255) not null default '', 
    standart_podvodki varchar (255) not null default '',
    razmer_rozetki varchar (255) not null default '',
    dopolnitelnie_funkcii varchar(1000) not null default '',
    osnaschenie varchar(255) not null default '',
    vraschenie_izliva varchar (255) not null default '',
    vidvizhnoi_izliv varchar (255) not null default '',
    vstroennii_kran_dlya_pitevoi_vodi  varchar (255) not null default '',
    donniy_klapan  varchar (255) not null default '',
    deviator  varchar (255) not null default '',
    zaschita_ot_obratnogo_potoka  varchar (255) not null default '',
    ogranichenie_temperaturi varchar (255) not null default '',
    funkciya_ekonomii_rashoda varchar (255) not null default ''
--
) engine=MyIsam default charset utf8 collate utf8_general_ci; 
alter table products_smesiteli add column rezhim_dusha_izliva varchar (255) not null default '';
alter table products_smesiteli add column uglovoi_ventil varchar (255) not null default '';
alter table products_smesiteli add column kolichestvo_potrebitelei varchar (255) not null default '';
alter table products_smesiteli add column istochnik_pitaniya_fotoelementa varchar (255) not null default '';


create table products_ogradi (
    id int(10) not null auto_increment primary key,
    product_id int(10) not null,
    external_id varchar(255) not null default '',
    seriya varchar(255) not null default '',
    model varchar(255) not null default '',
    proizvodstvo varchar(255) not null default '',
    country varchar (255) not null default '',
    price varchar(255) not null default '',
    osnaschenie varchar(255) not null default '',
-- 
    article_not_external_id varchar(255) not null default '',
    `type` varchar(255) not null default '', 
    montazh varchar (255) not null default '',
    width varchar(255) not null default '',
    height varchar(255) not null default '',
    glubina varchar(255) not null default '',
    garantiya varchar(255) not null default '',
    forma varchar(255) not null default '',
    oblast_primeneniya varchar(255) not null default '',
    design_style varchar(255) not null default '',
    image_description varchar(255) not null default '',
    dopinfo varchar(1000) not null default '',
    orientation varchar(255) not null default '',
--
    konstrukciya_dverei varchar(255) not null default '', 
    diametr_sliva varchar(255) not null default '',
    radius_poddona varchar(255) not null default '', 
    antiskolzyaschee_pokritie varchar(255) not null default '',    
    ispolnenie_polotna_dveri varchar(255) not null default '', 
    kolichestvo_sekcii_dveri varchar(255) not null default '',  
    tolschina_polotna_dveri varchar(255) not null default '',  
    cvet_profilya varchar(255) not null default '',
    cvet_poddona varchar(255) not null default '',
    glubina_poddona varchar(255) not null default '',
    visota_poddona varchar(255) not null default '',
    vid_poddona varchar(255) not null default '', 
    material_poddona varchar(255) not null default '', 
    material_polotna_dveri varchar(255) not null default '', 
    material_profilya varchar(255) not null default '', 
    reguliruemaya_shirina varchar(255) not null default '', 
    poddon_v_komplekte varchar(255) not null default '',
    dopolnitelnie_funkcii varchar(1000) not null default '' 
) engine=MyIsam default charset utf8 collate utf8_general_ci; 
alter table products_ugolki add column cvet_polotna_dveri varchar(255) not null default '';
--


create table product_images (
    id int(10) not null auto_increment primary key,
    product_id int(10) not null,
    url varchar(255) not null,
    filename varchar(255) not null,
    description varchar(255) not null
) engine=MyIsam default charset utf8 collate utf8_general_ci; 
alter table product_images add column product_external_id varchar(255) not null default '';
alter table product_images change column url url varchar(1000) not null;

-- запросы помощники для мониторинга 
select count(*) from products_unitazi;
select count(*) from products where category_id=5;
select * from log4php_log order by id desc limit 25;

select count(*) from proxy3 where active=1;
select count(*) from proxy3 where attempts>=3;

select * from products_unitazi where product_id=1823;
select count(*) from products_unitazi;
select count(*) from products where category_id=5;
select * from log4php_log order by id desc limit 25;

select count(*) from proxy3 where active=1;
select count(*) from proxy3 where attempts>=7;

select * from products_unitazi where product_id=1823;

select distinct(id) from product_images where url in 
('http://santehnika-online.ru/upload/resize_cache/iblock/33b/732_722_1/5a10f23d0698d2bce35b217da632e846.JPG',
'http://santehnika-online.ru/upload/iblock/065/cec61f945b367b1bb27d6409ea4c54ed.JPG',
'http://santehnika-online.ru/upload/iblock/df8/3c6b03ca991dedf6fc57d6a4377ada4f.JPG',
'http://santehnika-online.ru/upload/resize_cache/iblock/908/732_722_1/d82b3dfa9b788311afcd9f4258041d36.JPG',
'http://santehnika-online.ru/upload/iblock/82f/UNITAZY_ROCA_MATEO__N000224814_1.JPG',
'http://santehnika-online.ru/upload/resize_cache/iblock/ebb/732_722_1/c4a0e885086480f5ac72b5fd5d4f792a.JPG'

);

select count(*) from products_unitazi where label = '';

select products.label, products_unitazi.* from products
left join products_unitazi on products_unitazi.product_id = products.id;
