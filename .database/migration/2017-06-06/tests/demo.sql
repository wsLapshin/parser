
insert into containers (url, label, sys_label) values ('http://santehnika-online.ru/mebel_dlja_vannoj_komnaty/','Мебель для ванной комнаты', 'mebel_dlja_vannoj_komnaty');


insert into containers (url, label, sys_label) values ('http://santehnika-online.ru/vanny/akrilovye/','Акриловые ванны', 'akrilovye_vanni');

insert into containers (url, label, sys_label) values ('http://santehnika-online.ru/unitazy/','Унитазы', 'unitazy');

insert into items (external_id, container_id, url, label) values (
    '322363',
    '1',
    'http://santehnika-online.ru/product/mebel_dlya_vannoy_santa_omega_lyuks_65_podvesnaya_belaya_2_yashchika/',
    'Омега Люкс 65'
);

insert into items_property_demo (
    item_id,
    mtype,
    width,
    glubina,
    height,
    usefor,
    material_korpusa,
    pokritie_korpusa,
    material_fasada,
    pokritie_fasada,
    tumba,
    material_rakovini,
    moschnost_lampi,
    furniture,
    garantiya,
    forma_meb,
    forma_rakovini,
    hranenie_sys,
    design_style,
    color,
    color_rakovina,
    osnaschenie,
    dopinfo,
    montazh, 
    tip_lampi, 
    country,
    uglovaya,
    assimetry,
    seriya,
    proizvodstvo,
    price  
) values (
    '1',
    'комплект (гарнитур)',
    '65',
    '45',
    '52',
    'бытовая',
    'ДСП',
    'глянцевое, ламинат',
    'МДФ',
    'глянцевое, пленка',
    'нет',
    'искусственный мрамор',
    '15w',
    'хром',
    '3 года',
    'прямоугольная',
    'прямоугольная',
    'с дверками, с ящиками',
    'современный стиль',
    'белый',
    'белый',
    'крепления, механизм доводчика',
    'ящики полного выдвижения',
    'подвесной', 
    'галогеновая', 
    'Россия',
    'нет',
    'нет',
    'Омега',
    'СанТа',
    '14408'  
);

insert into items (external_id, container_id, url, label) values (
    '215999',
    '1',
    'http://santehnika-online.ru/product/mebel_dlya_vannoy_aquanet_latina_60_2_yashchika_belaya/',
    'Латина 60'
);

insert into items_property_demo (
    item_id,
    mtype,
    width,
    glubina,
    height,
    usefor,
    material_korpusa,
    pokritie_korpusa,
    material_fasada,
    pokritie_fasada,
    tumba,
    material_rakovini,
    moschnost_lampi,
    furniture,
    garantiya,
    forma_meb,
    forma_rakovini,
    hranenie_sys,
    design_style,
    color,
    color_rakovina,
    osnaschenie,
    dopinfo,
    montazh, 
    tip_lampi, 
    country,
    uglovaya,
    assimetry,
    seriya,
    proizvodstvo,
    price 
) values (
    '2',
    'комплект (гарнитур)',
    '60',
    '45',
    '65',
    'бытовая',
    'ДСП',
    'эмаль,глянцевое',
    'МДФ',
    'глянцевое, эмаль',
    'нет',
    'искусственный мрамор',
    '',
    'хром',
    '2 года',
    'прямоугольная',
    'прямоугольная',
    'с дверками, с ящиками',
    'современный стиль',
    'белый',
    'белый',
    'крепления, механизм доводчика',
    '2 ящика',
    'напольный, подвесной', 
    '', 
    'Россия',
    'нет',
    'нет',
    'Латина',
    'Aquanet',
    '27620' 
);

insert into item2images (item_id, url, filename, description) values (
    1,
    'http://santehnika-online.ru/upload/resize_cache/iblock/f23/732_722_1/1930e28c07dd2beffb515323d1277107.JPG',
    'Омега Люкс 65_1.jpg',
    'Продажа мебель для ванной Омега Люкс магазине santorg74'
),(
    1,
    'http://santehnika-online.ru/upload/iblock/cf8/311a842b1d753745c38144282995a263.JPG',
    'Омега Люкс 65_2.jpg',
    'Продажа мебель для ванной Омега Люкс магазине santorg74'
),
(
    2,
    'http://santehnika-online.ru/upload/iblock/cf8/311a842b1d753745c38144282995a263.JPG',
    'Латина 60_1.jpg',
    'Продажа мебель для ванной Латина 60 магазине santorg74'
);
select id, external_id, url, label from containers;
SELECT items.id, items.container_id, items.external_id, items.label, items.url, p.*   FROM `items` left join items_property_demo as p on p.item_id = items.id;
select items.external_id, item2images.url, item2images.filename, item2images.description from items left join item2images on item2images.item_id=items.id