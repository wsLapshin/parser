create database santeh default charset cp1251 collate cp1251_general_ci;
# Referer:http://santehnika-online.ru/mebel_dlja_vannoj_komnaty/
create table proxy(
    id int(11) not null auto_increment primary key,
    host varchar(15) not null,
    port smallint(5) unsigned not null default 8080,
    login varchar(255) not null default '',
    password varchar(255) not null default '',
    last_visited timestamp default current_timestamp,
    active tinyint(1) not null default 1,
    attempts tinyint(3) not null default 0
) engine=MyIsam default charset utf8;

create table log4php_log (
    id int(10) not null auto_increment primary key,
    timestamp timestamp not null default current_timestamp,
    level varchar(32) not null default '',
    logger varchar(256) not null default '',
    classify varchar(100) not null default '',
    message varchar(4000) not null default '',
    thread int(11) not null default 0,
    `file` varchar(256) not null default '',
     line varchar(10) not null default '',
    addition1 varchar(2000) not null default '',
    addition2 varchar(2000) not null default '',
    addition3 varchar(2000) not null default ''
) engine=MyIsam default charset utf8;

create table containers (
    id int(10) not null auto_increment primary key,
    sys_label varchar(255) not null default '',
    url varchar(255) not null default '',
    label varchar(255) not null default '',
    ixpath varchar(255) not null default '',
    ptempl varchar(255) not null default '',
    external_id varchar(255) not null default '',
) engine=MyIsam default charset cp1251; 

create table items (
    id int(10) not null auto_increment primary key,
    container_id int(10) not null,
    url varchar(255) not null default '',
    label varchar(255) not null default ''
    external_id varchar(255) not null default '',
) engine=MyIsam default charset cp1251;

create table items_property_demo (
    id int(10) not null auto_increment primary key,
    item_id int(10) not null,
    seriya varchar(255) not null default '',
    proizvodstvo varchar(255) not null default '',
    price varchar(255) not null default '',
    mtype varchar(255) not null default '' comment 'mebel type',
    width varchar(255) not null default '',
    glubina varchar(255) not null default '',
    height varchar(255) not null default '',
    usefor varchar(255) not null default '',
    material_korpusa varchar(255) not null default '',
    pokritie_korpusa varchar(255) not null default '',
    material_fasada varchar(255) not null default '',
    pokritie_fasada varchar(255) not null default '',
    tumba varchar(255) not null default '',
    material_rakovini varchar(255) not null default '',
    moschnost_lampi varchar(255) not null default '',
    furniture varchar(255) not null default '',
    garantiya varchar(255) not null default '',
    forma_meb varchar(255) not null default '',
    forma_rakovini varchar(255) not null default '',
    hranenie_sys varchar(255) not null default '',
    design_style varchar(255) not null default '',
    color varchar(255) not null default '',
    color_rakovina varchar(255) not null default '',
    osnaschenie varchar(255) not null default '',
    dopinfo varchar(255) not null default '',
    montazh varchar (255) not null default '',
    tip_lampi varchar (255) not null default '',
    country varchar (255) not null default '',
    uglovaya varchar(255) not null default '',
    assimetry varchar(255) not null default ''
) engine=MyIsam default charset cp1251;

create table item2images (
    id int(10) not null auto_increment primary key,
    item_id int(10) not null,
    url varchar(255) not null,
    filename varchar(255) not null,
    description varchar(255) not null
) engine=MyIsam default charset cp1251;