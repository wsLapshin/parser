<?php
use Doctrine\ORM\Tools\Console\ConsoleRunner;

include_once __DIR__ . '/../../src/config.php';

$doctrineConfig = Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration([ENTITY_DIR], DEV_MODE);
$entityManager = Doctrine\ORM\EntityManager::create([
    'driver' => MYSQL_DRIVER,
    'user' => MYSQL_USER,
    'password' => MYSQL_PASSWORD,
    'dbname' => MYSQL_DBNAME
], $doctrineConfig);

return ConsoleRunner::createHelperSet($entityManager);
