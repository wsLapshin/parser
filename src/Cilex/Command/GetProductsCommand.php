<?php

/*
 * This file is part of the Cilex framework.
 *
 * (c) Mike van Riel <mike.vanriel@naenius.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cilex\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Cilex\Provider\Console\Command;
use WsParser\Workflow;
use \Logger;

/**
 * Example command for testing purposes.
 */
class GetProductsCommand extends Command
{

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
                ->setName('getproducts')
                ->setDescription('parse products from collected with geturls command urls')
                ->addArgument('sys_label', InputArgument::REQUIRED, 'system identificator of parsing job, influenses the dir name from where mappers are got')
                ->addOption('start', 's', InputOption::VALUE_OPTIONAL, 'Start page number')
                ->addOption('finish', 'f', InputOption::VALUE_OPTIONAL, 'Finish page number');
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = Logger::getLogger(self::class);
        $di = $this->getContainer();
        $em = $di['em'];

        //cli args
        $sysLabel = $input->getArgument('sys_label');
        $mapper = 'product.xml';
        $mapperPath = PARSER_DIR . '/mappers/' . $sysLabel . '/' . $mapper;
        $subTable = 'products_' . $sysLabel;

        //parsing workflow
        $workflow = new Workflow($mapperPath, $this->getContainer());
        $start = (null === $input->getOption('start')) ? $workflow->getMinUrlPoolIndex() : $input->getOption('start');
        $finish = (null === $input->getOption('finish')) ? $workflow->getMaxUrlPoolIndex() : $input->getOption('finish');

        $db = $em->getConnection();

        for ($i = $start; $i <= $finish; $i++) {
            $cur_url = $workflow->getUrl($i)->getHref();
            $logger->info("process... " . $i . " [$cur_url] \n ---------------------------------------------");
            $cortege = $workflow->processOne($i);

            if (null !== $cortege) {
                if(empty($cortege['product_properties'])){
                    $logger->warn("unknown problem with proxy request [index$i]");
                    $i--;
                    continue;
                }
                //delete data in subtables
                $query = "delete from $subTable where product_id = :i";
                $stmt = $db->prepare($query);
                $stmt->execute([':i' => $i]);
                
                $query = "delete from product_images where product_id = :i";
                $stmt = $db->prepare($query);
                $stmt->execute([':i' => $i]);

                //prepare statements parts for saving product
                $fieldsPart = '';
                $valuesPart = '';
                $statementArray = [];
                $processedProperties = [];
                foreach ($cortege['product_properties'] as $property) {
                    $_key = array_keys($property);
                    $fieldName = $_key[0];
                    if (!in_array($fieldName, $processedProperties)) {
                        $processedProperties[] = $fieldName;
                        $fieldValue = $property[$fieldName];
                        $fieldsPart .= $fieldName . ', ';
                        $statementIndex = ':' . $fieldName;
                        $valuesPart .= $statementIndex . ', ';
                        $statementArray[$statementIndex] = $fieldValue;
                    }
                }
                $fieldsPart = substr($fieldsPart, 0, -2);
                $valuesPart = substr($valuesPart, 0, -2);
                $fieldsPart = '( product_id, price, image_description, ' . $fieldsPart . ') ';
                $valuesPart = 'values ( :product_id, :price, :image_description, ' . $valuesPart . ') ';
                $statementArray[":product_id"] = $i;
                $statementArray[":price"] = $cortege['product_common'][0]['price'];
                $statementArray[":image_description"] = 'Продажа ' . $cortege['product_common'][0]['label'];
                //save product
                $query = "insert into $subTable " . $fieldsPart . $valuesPart;
                $stmt = $db->prepare($query);
                $stmt->execute($statementArray);

                //save products' images urls
                foreach($cortege['product_images'] as $image){
                    $query = "insert into product_images (product_id, url, product_external_id) values (:product_id, :url, :product_external_id)";
                    $stmt = $db->prepare($query);
                    $external_id = $cortege['product_common'][0]['external_id'];
                    $href = $image['source'];
                    $stmt->execute([":product_id"=>$i, ":product_external_id"=>$external_id, ":url"=>$href]);
                }
            }
        }
    }

}
