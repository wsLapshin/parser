<?php

/*
 * This file is part of the Cilex framework.
 *
 * (c) Mike van Riel <mike.vanriel@naenius.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cilex\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Cilex\Provider\Console\Command;
use \Logger;
use \PDO;
use WsParser\ProxyCurl;
use WsParser\Document;
use WsParser\Exceptions\DocumentCurlException;
use WsParser\Exceptions\DocumentProxyException;
use WsParser\Exceptions\DocumentHttpException;
use \Exception;

/**
 * Example command for testing purposes.
 */
class GetImagesCommand extends Command
{

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
                ->setName('getimages')
                ->setDescription('parse images from product_image')
                ->addArgument('sys_label', InputArgument::REQUIRED, 'system identificator of parsing job, influenses the dir name from where mappers are got')
                ->addOption('start', 's', InputOption::VALUE_OPTIONAL, 'Start page number')
                ->addOption('finish', 'f', InputOption::VALUE_OPTIONAL, 'Finish page number');
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = Logger::getLogger(self::class);
        $di = $this->getContainer();
        $em = $di['em'];


        //cli args
        $sysLabel = $input->getArgument('sys_label');
        $imageTable = 'product_images';
//        $mapper = 'product.xml';
//        $mapperPath = PARSER_DIR . '/mappers/' . $sysLabel . '/' . $mapper;
//        $subTable = 'products_' . $sysLabel;
        //@todo make it with using workflow and xml configs

        //check output dir
        $outputPath = '/home/wiseman/www/http/santeh/src/WsParser/output';
        $savePath = $outputPath . '/' . $sysLabel;
        if (!file_exists($savePath)) {
            mkdir($savePath, 0777, true); //@todo try better rights
        } else {
            if (!is_dir($savePath) || !is_writable($savePath)) {
                throw new ContainerException("ImageSaveContainer: output path [$savePath] doesnt exists and cannt be created or is not writable");
            }
        }

        //make pool: get range for start and finish
        $db = $em->getConnection();
        $query = "select max(product_images.id) as `max`,
                         min(product_images.id) as `min`
                from $imageTable 
                    left join products on product_images.product_id = products.id
                    left join categories on products.category_id = categories.id
                    where categories.sys_label = :sysLabel";
        $stmt = $db->prepare($query);
        $stmt->execute([':sysLabel' => $sysLabel]);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $minIndex = $result[0]['min'];
        $maxIndex = $result[0]['max'];
        $start = (null === $input->getOption('start')) ? $minIndex : $input->getOption('start');
        $finish = (null === $input->getOption('finish')) ? $maxIndex : $input->getOption('finish');
        $subTable = 'products_' . $sysLabel;


        $query = "select product_images.id as image_id, 
                $subTable.external_id as external_id,
                product_images.url as url, 
                categories.sys_label as sys_label
        from $imageTable 
        left join $subTable on $subTable.product_id = product_images.product_id
        left join products on product_images.product_id = products.id
        left join categories on products.category_id = categories.id
        where categories.sys_label = :sysLabel and product_images.id>=:start and product_images.id<=:finish order by product_images.id asc";

        
        
        $stmt = $db->prepare($query);
        $stmt->execute([':sysLabel' => $sysLabel, ':start' => $start, ':finish' => $finish]);
//        $stmt->execute([':sysLabel' => $sysLabel]);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        

        if( count($result) == 0 ) {
            throw new Exception("no images found. check syslabel");
        }

        $curl = ProxyCurl::builder($em);
        //parsing workflow
        for ($i = 0; $i < count($result); $i++) {
            $row = $result[$i];
            $url_index = $row['image_id'];
            $url = $row['url'];

            $startTime = microtime(true);
            $logger->info("process... " . $url_index . " [$url] \n ---------------------------------------------");

            $baseName = $row['external_id'];
            $saveSubPath = $savePath . '/' . $baseName;
            if (!file_exists($saveSubPath)) {
                mkdir($saveSubPath, 0777, true); //@todo try better rights
            } else {
                if (!is_dir($saveSubPath) || !is_writable($saveSubPath)) {
                    throw new ContainerException("ImageSaveContainer: output path [$saveSubPath] doesnt exists and cannt be created or is not writable");
                }
            }

            //process document
            try {
                $document = new Document($url, $curl);
                $binary = $document->getImage();

                //save_file
                $filesList = scandir($saveSubPath);
                $index = 0;
                foreach ($filesList as $file) {
                    if (strpos($file, $baseName) === 0) {
                        $index++;
                    }
                }
                $newFilename = ($index == 0) ? $baseName . ".jpg" : $baseName . "_" . $index . ".jpg"; //@todo other mime
                $f = fopen($saveSubPath . '/' . $newFilename, 'w+b');
                $writeResult = fwrite($f, $binary);
                if( $writeResult === false ) { throw new Exception("error while writing file [$i]"); }
                fclose($f);

                //log
                $finishTime = microtime(true);
                $logger->info("finish process, filename [$newFilename] at " . date('Y-m-d H:i:s') . ", ended for: " . (string) ($finishTime - $startTime) . "\n\n");
            } catch (DocumentCurlException $e) {
                $logger->error("Document bad curl: index: [$url_index] [$url] \n" . $e->getMessage() . " code: " . $e->getCode() . "\n\n");
                throw $e;
            } catch (DocumentHttpException $e) {
                $logger->warn("Document bad HTTP response: index: [$url_index] [$url] \n" . $e->getMessage() . " code: [" . $e->getCode() . "]\n\n");
            } catch (DocumentProxyException $e) {
                $proxyStr = $curl->getProxy()->getHost() . ":" . $curl->getProxy()->getPort();
                $logger->info("Document bad proxy $proxyStr :[$url] \n" . $e->getMessage() . "\n\n");

                ProxyCurl::markUnreachable($em, $curl);
                unset($curl);
                $curl = ProxyCurl::builder($em);
                $i--;
                continue;
            } catch (Exception $e) {
                $logger->warn($e->getMessage());
            } finally {
                unset($document);
            }
        }
    }

}
