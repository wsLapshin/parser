<?php

namespace Cilex\Command;

use Symfony\Component\CssSelector\CssSelectorConverter;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Cilex\Provider\Console\Command;

/**
 * Description of Css2xpathCommand
 *
 * @author wiseman
 */
class Css2xpathCommand extends Command 
{
    protected function configure()
    {
        $this
            ->setName('css2xpath')
            ->setDescription('helper to convert css selectors to xpath')
            ->addArgument('cssSelector', InputArgument::REQUIRED, 'css selector to convert');
    }

    
    protected function execute(InputInterface $input, OutputInterface $output){
       $cssSelector = $input->getArgument('cssSelector');
       $converter = new CssSelectorConverter();
       $output->writeln($converter->toXPath($cssSelector));
    }

}
