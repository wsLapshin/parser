<?php

/*
 * This file is part of the Cilex framework.
 *
 * (c) Mike van Riel <mike.vanriel@naenius.com>
 *
 * For the full copyright and license information, please view the LICENSE
 *file that was distributed with this source code.
 */

namespace Cilex\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Cilex\Provider\Console\Command;
use WsParser\Workflow;
use \Logger;

/**
 * Example command for testing purposes.
 */
class GetUrlCommand extends Command
{

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
                ->setName('geturl')
                ->setDescription('get product urls from category container')
                ->addArgument('sys_label', InputArgument::REQUIRED, 'system identificator of parsing job, influenses the dir name from where mappers are got')
                ->addOption('start', 's', InputOption::VALUE_OPTIONAL, 'Start page number')
                ->addOption('finish', 'f', InputOption::VALUE_OPTIONAL, 'Finish page number');
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = Logger::getLogger(self::class);
        $di = $this->getContainer();
        $em = $di['em'];

        //cli args
        $sysLabel = $input->getArgument('sys_label');
        $mapper = 'url.xml';
        $mapperPath = PARSER_DIR . '/mappers/' . $sysLabel . '/' . $mapper;

        //check if category exists
        $db = $em->getConnection();
        $query = "select id from categories where sys_label = :sys_label";
        $stmt = $db->prepare($query);
        $stmt->execute([':sys_label' => $sysLabel]);
        $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        if (count($rows) > 0) {
            $saveCategory = false;
            $category_id = $rows[0]['id'];
        } else {
            $saveCategory = true;
            $category_id = null;
        }


        //parsing workflow
        $workflow = new Workflow($mapperPath, $this->getContainer());
        $start = (null===$input->getOption('start'))?$workflow->getMinUrlPoolIndex():$input->getOption('start');
        $finish = (null===$input->getOption('finish'))?$workflow->getMaxUrlPoolIndex():$input->getOption('finish');

        for ($i = $start; $i <= $finish; $i++) {
            $cur_url = $workflow->getUrl($i)->getHref();
            $logger->info("process... " . $i ." [$cur_url] \n ---------------------------------------------");
            $cortege = $workflow->processOne($i);
//            var_dump($cortege);

            if (null !== $cortege) {

                //сохраняем категорию,если не была сохранена ранее или не существует
                if ($saveCategory) {
                    $query = "insert into categories (sys_label, last_url, label) values (:sys_label, :url, :label)";
                    $stmt = $db->prepare($query);
                    $stmt->execute([
                        ':sys_label' => $sysLabel,
                        ':url' => $cortege['categories'][0]['current_url'],
                        ':label' => $cortege['categories'][0]['label']
                    ]);
                    $category_id = $db->lastInsertId();
                    $saveCategory = false;
                } else {
                    $query = "update categories set last_url=:url where sys_label=:sys_label";

                    $stmt = $db->prepare($query);
                    $stmt->execute([
                        ':url' => $cortege['categories'][0]['current_url'],
                        ':sys_label' => $sysLabel,
                    ]);
                }

                //сохраняем урлы на продукты
                foreach ($cortege['products'] as $product) {
                    $query = "insert into products (category_id, url, label, external_id, category_page) values (:category_id, :url, :label, :external_id, :category_page) ";

                    $stmt = $db->prepare($query);
                    $stmt->execute([
                        ':category_id' => $category_id,
                        ':category_page' =>$i,                
                        ':label' => $product['lable'],
                        ':url' => $product['url'],
                        ':external_id' => $product['external_id'],
                    ]);
                }
            }
        }

//        //save given container
//        $productCategory = new ProductCategory();
//        $productCategory->setSysLabel($sysLabel);
//        $productCategory->setIxpath($xpathItem);
//        $productCategory->setUrl($url);
//        $productCategory->setPtempl($pageUrlTemplate);
//        if( null!==$containerMap->getLabel()){
//            $productCategory->setLabel($containerMap->getLabel());
//        }
//        
//        $em->persist($productCategory);
//        $em->flush();
//        $categoryId = $productCategory->getId();
//
        //       $output->writeln($return);
    }

}
