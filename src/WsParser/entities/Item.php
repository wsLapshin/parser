<?php 

namespace WsParser\Entity;

/**
 * @Entity
 * @Table(name="items")
 */
class Item
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue 
     */
    protected $id;
    
    /**
     * @Column(name="external_id")
     * unique
     */
    protected $externalId='';
   
    /**
     * @Column(type="integer", name="container_id")
     */
    protected $containerId;

    /**
     * @Column(nullable=FALSE)
     */
    protected $url;

    /** @Column */
    protected $label = '';

    public function getId()
    {
        return $this->id;
    }

    public function getExternalId()
    {
        return $this->externalId;
    }

    public function getContainerId()
    {
        return $this->containerId;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    public function setContainerId($containerId)
    {
        $this->containerId = $containerId;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }


}