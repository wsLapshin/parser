<?php
namespace WsParser\Entity;

/**
 * @Entity
 * @Table(name="containers")
 */
class Container
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue 
     */
    protected $id;

    /**
     * @Column(type="string", name="external_id")
     * unique 
     */
    protected $externalId = '';

    /** @Column */
    protected $url = '';

    /** @Column(name="sys_label") */
    protected $sysLabel = '';

    /** @Column */
    protected $label = '';

    /** @Column */
    protected $ixpath = '';

    /** @Column */
    protected $ptempl = '';

    /** @Column */
    protected $mapper = '';

    public function getId()
    {
        return $this->id;
    }

    public function getExternalId()
    {
        return $this->externalId;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getSysLabel()
    {
        return $this->sysLabel;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getIxpath()
    {
        return $this->ixpath;
    }

    public function getPtempl()
    {
        return $this->ptempl;
    }

    public function getMapper()
    {
        return $this->mapper;
    }

    public function setExternalId(type $externalId)
    {
        $this->externalId = $externalId;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function setSysLabel($sysLabel)
    {
        $this->sysLabel = $sysLabel;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function setIxpath($ixpath)
    {
        $this->ixpath = $ixpath;
    }

    public function setPtempl($ptempl)
    {
        $this->ptempl = $ptempl;
    }

    public function setMapper($mapper)
    {
        $this->mapper = $mapper;
    }

}
