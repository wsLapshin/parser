<?php

namespace WsParser\Entity;

/**
 * @Entity
 * @Table(name="proxy")
 */
class Proxy
{

    /**
     * @Id @Column(type="integer")
     * @GeneratedValue 
     */
    protected $id;

    /**
     * @Column(type="string")
     */
    protected $host;

    /** @Column (type="integer") */
    protected $port = 8080;

    /** @Column(type="string") */
    protected $login = '';

    /** @Column(type="string") */
    protected $password = '';

    /** @Column (type="datetime", name="last_visited") */
    protected $last_visited;

    /** @Column (type="integer")*/
    protected $active = 1;
    
    /** @Column (type="integer")*/
    protected $attempts = 0;

    public function __construct()
    {
        $this->last_visited = new \DateTime();
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getAttempts()
    {
        return $this->attempts;
    }

    public function setAttempts($num)
    {
        $this->attempts = $num;
    }
    
    public function getHost()
    {
        return $this->host;
    }

    public function getPort()
    {
        return $this->port;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getLast_visited()
    {
        return $this->last_visited;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setHost($host)
    {
        $this->host = $host;
    }

    public function setPort($port)
    {
        $this->port = $port;
    }

    public function setLogin($login)
    {
        $this->login = $login;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function setLast_visited($last_visited)
    {
        $this->last_visited = $last_visited;
    }

    public function setActive($active)
    {
        $this->active = $active;
    }


}
