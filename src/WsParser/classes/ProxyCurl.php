<?php
namespace WsParser;

use \Curl\Curl as BaseCurl;
use \PDO;
use Doctrine\ORM\EntityManager;
use WsParser\Entity\Proxy;
use \Logger;
use WsParser\Exceptions\ProxyCurlException;

class ProxyCurl extends BaseCurl
{
    const MAX_PROXY_ATTEMPTS = 50;//5

    /* do requests in 4 thread min*/
    /* all in seconds */
    const IP_TIMEOUT = 3; //10
    const CONNECTION_TIMEOUT = 3;//1.5
    const REQUEST_TIMEOUT = 10;//6
    const IMAGE_REQUEST_TIMEOUT = 10;
    
    private $userAgents = [

        'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36', //google Chrome
        'Mozilla/5.0 (Windows; U; Windows NT 5.0; it-IT; rv:1.7.12) Gecko/20050915', //Mozilla Firefox
        'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36 OPR/32.0.1948.25', //Opera
        /*'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.82 Safari/537.36 Edge/14.14359',//Microsoft Edge
        'Mozilla/5.0 (IE 11.0; Windows NT 6.3; WOW64; Trident/7.0; Touch; rv:11.0) like Gecko', //Internet Explorer
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13) AppleWebKit/603.1.13 (KHTML, like Gecko) Version/10.1 Safari/603.1.13', //Safari
        'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1104.222 YaBrowser/1.5.1104.222 Safari/537.4', //Yandex Browser */
        'Googlebot/2.1 (+http://www.googlebot.com/bot.html)',//Google Bot
        'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',//Google Bot
        'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',//Google Bot
        'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',//Google Bot
        'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', //Yandex Bot
        'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', //Yandex Bot
        'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', //Yandex Bot
        'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)' //Yandex Bot
    ];

    private $last_visited;
    private $proxy;

    public function __construct(Proxy $proxy, $base_url = null)
    {
        parent::__construct($base_url);
        $host = $proxy->getHost();
        $port = $proxy->getPort();
        $this->last_visited = $proxy->getLast_visited();
        $this->proxy = $proxy;
        $this->setOpt(CURLOPT_PROXY, $host . ':' . $port);
        $this->setOpt(CURLOPT_CONNECTTIMEOUT_MS, self::CONNECTION_TIMEOUT*1E3);
        $this->setOpt(CURLOPT_TIMEOUT_MS, self::REQUEST_TIMEOUT*1E3);
        $this->setOpt(CURLOPT_AUTOREFERER, true);
        $this->setOpt(CURLOPT_FOLLOWLOCATION, true);
        if( !empty($proxy->getLogin) && !empty($proxy->getPassword) ){
            $username = $proxy->getLogin();
            $password = $proxy->getPassword();
            $this->setOpt(CURLOPT_PROXYUSERPWD, $username . ':' . $password);
        }
    }

    public function getLastVisited()
    {
        return $this->last_visited;
    }

    public function generateUserAgent()
    {
        $max = count($this->userAgents) - 1;
        $index = rand(0, $max);
        $this->setUserAgent($this->userAgents[$index]);
        return $this->userAgents[$index];
    }

    public function checkCurlAvaiable()
    {
        return true;
    }

    public function getProxy()
    {
        return $this->proxy;
    }
    
    /**
     * @todo Переписать на нормальный doctrine, затыка в том что doctrine не поддерживает unix_timestamp()
     * @param EntityManager $em
     * @return WsParser\ProxyCurl
     */
    static function builder(EntityManager $em,  $doubleCheck = false)
    {
        $curl = null;
 
        $logger = Logger::getLogger(self::class);
        $query = "select * from proxy3 where (unix_timestamp() - unix_timestamp(last_visited) ) > :ip_timeout and active=1 order by last_visited asc limit 1";
        $stmt = $em->getConnection()->prepare($query);
        $stmt->execute([':ip_timeout'=> self::IP_TIMEOUT]);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if( count($rows)>0 ){
            $row = $rows[0];
            
            $proxy = new Proxy();
            $proxy->setHost($row['host']);
            $proxy->setPort($row['port']);
            $proxy->setActive($row['active']);
            $proxy->setLogin($row['login']);
            $proxy->setPassword($row['password']);
            $proxy->setAttempts($row['attempts']);

            $last_visited = date('Y-m-d H:i:s');
            $proxy->setLast_visited($last_visited);
            $id = $row['id'];
            $proxy->setId($id);

            $curl = new ProxyCurl($proxy);
            $curl->generateUserAgent();
            
            $query = "update proxy3 set last_visited=:last_visited where id=:id";
            $stmt = $em->getConnection()->prepare($query);
            $stmt->execute([':last_visited'=>$last_visited, ':id'=>$id]);
            $message = 'ProxyCurlBuilder: got curl with proxy: ' . $curl->getOpt(CURLOPT_PROXY) . "\n user agent: " . $curl->getOpt(CURLOPT_USERAGENT);
            $logger->info($message);
        } else {
            if(!$doubleCheck){
                $logger->debug("no free proxy sleep a little ...");
                usleep(self::IP_TIMEOUT * 1E6);
                $curl = self::builder($em, true);//$doublecheck = true to avoid recursion
            }

            if (null === $curl) {
                $message = "There is no active or time avaiable proxy servers in proxy list (db)";
                $logger->error($message);
                throw new ProxyCurlException("Workflow: " . $message);
            }
        }
        return $curl;
    }

    static function markUnreachable(EntityManager $em, ProxyCurl $curl )
    {
       $proxy = $curl->getProxy();
       $attempts = $proxy->getAttempts()+1;
       if( $attempts >= self::MAX_PROXY_ATTEMPTS){
           $proxy->setActive(0);
       }
        $query = "update proxy3 set attempts=:attempts, active=:active where id = :id"; 
        $stmt = $em->getConnection()->prepare($query);
        $stmt->execute([':attempts'=>$attempts, ':active'=>$proxy->getActive(), ':id'=>$proxy->getId()]);
        $curl->close();
    }

    static function markReachable(EntityManager $em, ProxyCurl $curl)
    {
       $proxy = $curl->getProxy();
       $attempts = 0; 
        $query = "update proxy3 set attempts=:attempts, active=:active where id = :id"; 
        $stmt = $em->getConnection()->prepare($query);
        $stmt->execute([':attempts'=>$attempts, ':active'=>1, ':id'=>$proxy->getId()]);
        $curl->close();
        
    }
}
