<?php

namespace WsParser;

use WsParser\Exceptions\DocumentCurlException;
use WsParser\Exceptions\DocumentProxyException;
use WsParser\Exceptions\DocumentHttpException;
use WsParser\ProxyCurl;
use SimpleXMLElement;

class Document
{

    const DOUBLE_CHECK_CURL_ERROR = true;

    private $curl;
    private $url;
    private $charset;
    private $XHTML;
    private $tidyConf;

    public function __construct($url, ProxyCurl $curl, $charset = 'UTF8')
    {
        $this->url = $url;
        $this->charset = $charset;
        $this->tidyConf = [
            'indent' => true,
            'output-xhtml' => true,
            'bare' => true,
            'wrap' => 200];
        $this->curl = $curl;
    }

    public function getXHTML()
    {
        $html = $this->doRequest();
        $XHTML = $this->doXMLClean($html);
        $this->XHTML = $XHTML;
        return $XHTML;
    }

    public function getImage()
    {
        $this->curl->setOpt(CURLOPT_HEADER, 0);
        $this->curl->setOpt(CURLOPT_BINARYTRANSFER, 1);
        $this->curl->setOpt(CURLOPT_TIMEOUT_MS, ProxyCurl::IMAGE_REQUEST_TIMEOUT * 1E3);
        //@todo reset options
        $raw = $this->doRequest();
        return $raw;
    }

    public function doRequest()
    {
        $logger = \Logger::getLogger(self::class);
        $this->curl->get($this->url);

        //check http error
        if ($this->curl->httpError) {
            //some http errors are because of bad proxy for specific site
            if( in_array($this->curl->httpStatusCode,[429,403]) ){
                throw new DocumentProxyException($this->curl->httpErrorMessage, $this->curl->httpStatusCode);
            } else {
                throw new DocumentHttpException($this->curl->httpErrorMessage, $this->curl->httpStatusCode);
            }
        }

        //check curl error
        if ($this->curl->curlError) {
            //something wrong with proxy 
            if (in_array($this->curl->curlErrorCode, [5,6,7,9,18,28,52,55,56])) {
                throw new DocumentProxyException($this->curl->curlErrorMessage, $this->curl->curlErrorCode);
            } else {
                throw new DocumentCurlException($this->curl->curlErrorMessage, $this->curl->curlErrorCode);
            }
        }

        $html = $this->curl->response;

        //proxy processed bad response, may be http error
        if( $this->curl->getOpt(CURLOPT_BINARYTRANSFER) != 1 ) {
            if (strlen(trim($html)) < 100 ){
                throw new DocumentProxyException('Proxy sends empty response', -1); 
               /* 
                $logger->info("double check bad responce.....\n");

                //try without proxy
                $this->curl->setOpt(CURLOPT_PROXY, null);
                $this->curl->get($this->url);

                if($this->curl->httpError){
                    throw new DocumentHttpException($this->curl->httpErrorMessage, $this->curl->httpStatusCode);
                }

               $html = $this->curl->response; */
            }
        }

        return $html;
    }

    private function doXMLClean($html)
    {
        $tidy = tidy_parse_string($html, $this->tidyConf, 'UTF8');
        $res = $tidy->cleanRepair();
        $tidy = preg_replace("/xmlns=\"[^\"]*\"/", "", $tidy);
        $XHTML = new SimpleXMLElement($tidy);
        return $XHTML;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getCharset()
    {
        return $this->charset;
    }

    public function getTidyConf()
    {
        return $this->tidyConf;
    }

    public function setUrl($url)
    {
        $this->currentXml = null;
        $this->url = $url;
    }

    public function setCharset($charset)
    {
        $this->currentXml = null;
        $this->charset = $charset;
    }

    public function getLastCurl()
    {
        return $this->curl;
    }

}
