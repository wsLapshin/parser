<?php

namespace WsParser\Containers;

use WsParser\Interfaces\IContainer;
use SimpleXMLElement;
use WsParser\Exceptions\ContainerException;

/**
 * @todo Сделать наследников класса immutable, переопределив магические методы
 */
abstract class AContainer implements IContainer
{

    //SimpleXMLElement of current configs' <container> tag
    protected $containerConf;
    protected $attributes;
    protected $additional;
    //SimpleXMLElement of relative parsed container or whole Document
    protected $context;
    protected $fields;
    protected $cortege;
    //String Xpath to find containers in XHTML
    protected $xpath;
    //String Name of current container
    protected $name;

    public function __construct(SimpleXMLElement $configXMLContainer, SimpleXMLElement $context, $additional)
    {
        $this->context = new SimpleXMLElement($context->asXML());
        $this->containerConf = $configXMLContainer;
        $this->attributes = $configXMLContainer->attributes();
        $this->additional = $additional;
        $this->xpath = (isset($configXMLContainer['xpath']) ? (string) $configXMLContainer['xpath'] : '//body');
        if (!isset($configXMLContainer['name'])) {
            throw new ContainerException('Container tag must have name');
        }
        $this->name = (string) $configXMLContainer['name'];
        $this->fields = [];

        $configXMLFields = $configXMLContainer->field;

        //process fields
        foreach ($configXMLFields as $configXMLField) {
            if (!isset($configXMLField['name'])) {
                throw new ContainerException("In contaner [$this->name] there is a field without name");
            }

            $fieldName = (string) $configXMLField['name'];


            if (isset($configXMLField['xpath'])) {
                $fieldsXML = $this->context->xpath((string) $configXMLField['xpath']);
                
                //field should have only one value in context of container
                if (count($fieldsXML) == 0) {
                    $fieldValue = '';
                } elseif (count($fieldsXML) == 1) {

                    $fieldXML = $fieldsXML[0];

                    if (isset($configXMLField['attr'])) {
                        $attribute = (string) $configXMLField['attr'];
                        $fXML = (array) $fieldXML;
                        $fieldValue = $fXML['@attributes'][$attribute];
                    } else {
                        $fieldValue = $this->getTextNode($fieldXML);
                    }
                } else {
                    throw new ContainerException("In container [$this->name] there is a field [$fieldName] with arbitary xpath (gives more than one value) ");
                }
            } elseif (isset($configXMLField['value'])) {
                $fieldValue = (string) $configXMLField['value'];
            } else {
                throw new ContainerException("In container [$this->name] field [$fieldName] there is a field without value or xpath");
            }

            //mutate fieldValue if need
            if (isset($configXMLField['mutator'])) {

                $mutatorsChain = explode('~', (string)$configXMLField['mutator'] );

                foreach($mutatorsChain as $mutator){
                    $mutatorAr = explode('.', $mutator);

                    if (count($mutatorAr) == 1) {
                        $className = 'WsParser\Mutators\DefaultMutator';
                        $method = $mutatorAr[0];
                    } else {
                        $className = $mutatorAr[0];
                        $method = $mutatorAr[1];
                    }

                    $attributes = $configXMLField->attributes();

                    if (!is_callable([$className, $method])) {
                        throw new ContainerException("In container [$this->name] field [$fieldName] mutator couldn't be called");
                    }

                    $mutator = new $className($fieldValue, $attributes);
                    $fieldValue = $mutator->{$method}();
                }
            }

            $this->fields[$fieldName] = $fieldValue;
        }
        $this->init();
    }

    abstract protected function init();

    public function getCortege()
    {
        return $this->cortege;
    }

    public function getFields()
    {
        return $this->fields;
    }

    public function getName()
    {
        return $this->name;
    }

    private function getTextNode(SimpleXMLElement $parent)
    {
        $parentStr = $parent->asXML();
        return strip_tags($parentStr);
    }
}
