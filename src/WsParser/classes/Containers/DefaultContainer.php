<?php
namespace WsParser\Containers;

use WsParser\Containers\AContainer;

final class DefaultContainer extends AContainer
{
    protected function init()
    {
       $this->cortege = $this->fields; 
    }
}