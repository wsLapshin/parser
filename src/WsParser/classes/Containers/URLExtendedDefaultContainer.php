<?php
namespace WsParser\Containers;

use WsParser\Containers\AContainer;

final class URLExtendedDefaultContainer extends AContainer
{
    protected function init()
    {
       $urlWrap = $this->additional['currenturl'];
       $url = $urlWrap->getHref();
       $url_index = $urlWrap->getPoolIndex();
       $url_sys_label = $urlWrap->getSysLabel();
       $this->cortege = $this->fields; 
       $this->cortege['current_url'] = $url;
       $this->cortege['url_index'] = $url_index;
       $this->cortege['url_sys_label'] = $url_sys_label;
    }
}