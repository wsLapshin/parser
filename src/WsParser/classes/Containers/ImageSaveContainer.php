<?php

namespace WsParser\Containers;

use WsParser\Containers\AContainer;
use WsParser\Document;
use WsParser\Exceptions\ContainerException;
use WsParser\Exceptions\DocumentCurlException;
use WsParser\Exceptions\DocumentHttpException;
use WsParser\Exceptions\DocumentProxyException;
use \Logger;

final class ImageSaveContainer extends AContainer
{

    protected function init()
    {
        $outputPath = $this->attributes['output'];

        $urlWrap = $this->additional['currenturl'];
        $curl = $this->additional['curl'];
        $em = $this->additional['em'];
        
        $logger = Logger::getLogger(self::class);

        //prepare saving
        $sys_label = $urlWrap->getSysLabel();
        $savePath = $outputPath . '/' . $sys_label;

        //check base outputdir
        if (!file_exists($savePath)) {
            mkdir($savePath, 0777, true); //@todo try better rights
        } else {
            if (!is_dir($savePath) || !is_writable($savePath)) {
                throw new ContainerException("ImageSaveContainer: output path [$savePath] doesnt exists and cannt be created or is not writable");
            }
        }


        //find already saved  
        $baseName = $sys_label;
        $filesList = scandir($savePath);
        $index = 0;
        foreach ($filesList as $file) {
            if (strpos($file, $baseName) === 0) {
                $index++;
            }
        }
        $newFilename = ($index == 0) ? $baseName . ".jpg" : $baseName . "_" . $index . ".jpg"; //@todo other mime
        $f = fopen($savePath . '/' . $newFilename, 'w+');

        //save
        $src = $this->fields['source'];
        try {
            $proxy2 = $curl->getProxy();
            $curl2 = new \WsParser\ProxyCurl($proxy2);
//           $curl2->download($src, $newFilename); 
            $document = new Document($src, $curl2);
            $bytes = $document->getImage();
            fwrite($f, $bytes);
        } catch (DocumentCurlException $e) {
            throw $e; //todo, should throw ContainerException
        } catch (DocumentHttpException $e) {
            $logger->warn("Document bad IMAGE request: [$src] " . $e->getMessage() . "\n\n");
        } catch (DocumentProxyException $e) {
            //get curl
            try {
                $this->additional['curl'] = ProxyCurl::builder($em);
                return $this->init();
            } catch (ProxyCurlException $e) {
                $logger->error("Document IMAGE no proxy for: [$url]" . $e->getMessage() . "\n\n");
                throw $e;
            }
        } finally {
            fclose($f);
        }

        //form cortege
        $this->cortege['save_path'] = $savePath;
        $this->cortege['filename'] = $newFilename; 
        $this->cortege['source'] = $this->fields['source']; 

        return true;
    }

}
