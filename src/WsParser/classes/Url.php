<?php

namespace WsParser;

use WsParser\Exceptions\UrlException;

class Url
{
    private $poolIndex;
    private $sysLabel;
    private $href;

    public function __construct($href, $poolIndex = null, $sysLabel = null)
    {
       $this->poolIndex = $poolIndex;
       $this->sysLabel = $sysLabel;
       if (!$this->validateUrl($href)){
           throw new UrlException("Url: href [$href] is not valid http url");
       }
       $this->href = $href;
    }

    public function getPoolIndex()
    {
        return $this->poolIndex;
    }

    public function getSysLabel()
    {
        return $this->sysLabel;
    }

    public function getHref()
    {
        return $this->href;
    }

    public function setPoolIndex($poolIndex)
    {
        $this->poolIndex = $poolIndex;
    }

    public function setSysLabel($sysLabel)
    {
        $this->sysLabel = $sysLabel;
    }

    public function setHref($href)
    {
        $this->href = $href;
    }

    /**
     * @todo
     */
    private function validateUrl($url)
    {
        return true;
    }

}