<?php

namespace WsParser\Mutators;

abstract class AMutator
{
    protected $attributes = [];
    protected $value;
    
    public function __construct($value, $attributes)
    {
        $this->attributes = $attributes;
        $this->value = $value;
    }

    public function getAttribute($index)
    {
        return $this->attributes[$index];
    }
}