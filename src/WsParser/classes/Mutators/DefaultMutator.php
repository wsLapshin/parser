<?php

namespace WsParser\Mutators;

use WsParser\Mutators\AMutator;

class DefaultMutator extends AMutator 
{
   public function prefix()
   {
       return $this->getAttribute('prefix') . $this->value;
   }

   public function trim()
   {
       return trim($this->value);
   }

   public function stripTags()
   {
       return strip_tags($this->value);
   }

   public function htmlEntityDecode()
   {
       return html_entity_decode($this->value);
   }

   public function stripLongSpaces()
   {
       return preg_replace('/\s{1,}/', ' ', $this->value);
   }

   public function normalizePrice()
   {
       //normalize ,
       $val = str_replace(',', '.', $this->value);

       //normalize spaces
       $val = str_replace(' ', '', $val);

       return (float)$val;
   }

   public function lowercase()
   {
       return mb_strtolower($this->value);
   }
}