<?php

namespace WsParser\UrlResources;

use WsParser\Interfaces\IUrlResource;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use \PDO;
use SimpleXMLElement;
use WsParser\Url;


class DbItemsUrlResource implements IUrlResource
{
    public function getUrls(SimpleXMLElement $configXMLDocument)
    {
        $category_sys_label = $configXMLDocument['category_sys_label'];
        $doctrineConfig = Setup::createAnnotationMetadataConfiguration([ENTITY_DIR], DEV_MODE);
        $em = EntityManager::create([
            'driver' => MYSQL_DRIVER,
            'user' => MYSQL_USER,
            'password' => MYSQL_PASSWORD,
            'dbname' => MYSQL_DBNAME
        ], $doctrineConfig);
        $query = "select p.url, p.id, p.external_id from products as p left join categories as c on c.id = p.category_id where c.sys_label = :category_sys_label"; 
        
        $stmt = $em->getConnection()->prepare($query);
        $stmt->execute([':category_sys_label'=>$category_sys_label]);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $result = [];
        
        foreach($rows as $row){
            $index = $row['id'];
            $sys_label = $row['external_id']; //sys_label of product, used in ImageSaveContainer later
            $href = $row['url'];
            $urlWrap = new Url($href, $index, $sys_label);
            $result[$index] = $urlWrap;
        }

        return $result;
    }

}