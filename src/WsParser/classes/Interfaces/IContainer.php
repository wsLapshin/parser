<?php
namespace WsParser\Interfaces;

use SimpleXMLElement;

interface IContainer
{
    
    function __construct(SimpleXMLElement $configXMLContainer, SimpleXMLELement $context, $additional);

    function getName();
    
    function getFields();

    function getCortege();
}