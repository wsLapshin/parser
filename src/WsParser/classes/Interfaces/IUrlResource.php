<?php
namespace WsParser\Interfaces;

use SimpleXMLElement;

interface IUrlResource
{
    /**
     * @param SimpleXMLElement $configXMLDocument - конфигурационный тег документа
     * @return WsParser\Url array[] - массив ссылок для обработки
     */
    function getUrls(SimpleXMLElement $configXMLDocument);
    
}