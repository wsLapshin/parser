<?php

namespace WsParser;

use WsParser\Exceptions\WorkflowException;
use WsParser\Exceptions\DocumentCurlException;
use WsParser\Exceptions\DocumentProxyException;
use WsParser\Exceptions\DocumentHttpException;
use WsParser\Exceptions\ContainerException;
use WsParser\Exceptions\UrlException;
use WsParser\Exceptions\ProxyCurlException;
use WsParser\Document;
use WsParser\ProxyCurl;
use \Logger;
use SimpleXMLElement;
use WsParser\Url;

//use WsParser\Interfaces;

class Workflow
{

    const REQUEST_PAUSE = 0;

    private $configXML;
    private $configPath;
    private $urlPool;
    private $start;
    private $finish;
    private $corteges = [];
    private $lastProcessedIndex;
    private $lastProcessedDocument;
    private $lastProcessedCurl;
    private $lastProcessedCorteges = [];
    private $di;
    private $logger;

    public function __construct($configPath, $di)
    {
        //globals
        $this->di = $di; //dependency injection container
        $this->logger = Logger::getLogger(self::class);

        // load xml mapper file
        if (!file_exists($configPath)) {
            $message = "Mapper [$configPath] does not exist";
            $this->logger->error('Workflow: ' . $message);
            throw new WorkflowException("Mapper [$configPath] does not exist");
        }
        $this->configPath = $configPath;

        libxml_clear_errors();
        $oldValue = libxml_use_internal_errors(true);

        // Load XML
        $configXML = @simplexml_load_file($configPath);
        if ($configXML === false) {

            $errorStr = "";
            foreach (libxml_get_errors() as $error) {
                $errorStr .= $error->message;
            }
            $message = "Error loading mapping [$configPath]: " . trim($errorStr);
            $this->logger->error('Workflow: ' . $message);
            throw new WorkflowException($message);
        }

        libxml_clear_errors();
        libxml_use_internal_errors($oldValue);

        $this->configXML = $configXML;

        // construct urls pool
        if (!isset($configXML->document)) {
            $message = "Mapping [$configPath] must have one tag 'document' in root tag";
            $this->logger->error('Workflow: ' . $message);
            throw new WorkflowException($message);
        }

        $this->urlPool = [];

        if (isset($configXML->document['res'])) {
            $this->makeUrlPoolFromResource($configXML->document);
        } elseif (isset($configXML->document['tpl']) &&
                isset($configXML->document['start']) &&
                isset($configXML->document['step']) &&
                isset($configXML->document['max'])) {

            $this->makeUrlPoolByTemplate($configXML->document);
        } elseif (isset($configXML->document['url'])) {

            $sys_label = (null === $configXML->document['url']) ? null : $configXML->document['url'];
            try {
                $urlWrap = new Url($configXML->document['url'], 0, $sys_label);
                $this->urlPool[] = $urlWrap;
            } catch (UrlException $e) {
                $message = $e->getMessage();
                $this->logger->warn("Building URL: given url [$url] is not valid url. " . $message);
            }
        } else {

            $message = "Mapping file [$configPath] doesnt have url or tpl or res attribute in document tag (used for constructing pull of documents' jos";
            $this->logger->error("Document: " . $message);
            throw new WorkflowException($message);
        }

        $this->setInterval();
    }

    /**
     * Parsing of interval of urls
     * Interval defined according $this->start and $this->finish properties
     */
    public function processInterval()
    {
        for ($i = $this->getStart(); $i <= $this->getFinish(); $i++) {
            $this->processOne($i);
            usleep(self::REQUEST_PAUSE * 1E6);
        }

        return $this->getCorteges();
    }

    /**
     * Parsing the explicit url
     * @param int $index - index of url in $this->urlPool
     * @return array - lastprocessed cortege
     */
    public function processOne($index)
    {
        $charset = ( null !== $this->configXML->document['charset'] ) ? $this->configXML->document['charset'] : 'UTF8';
        $this->lastProcessedIndex = $index;
        $this->lastProcessedDocument = null;
        $this->lastProcessedCorteges = null;
        
        //get curl
        try {
            $curl = ProxyCurl::builder($this->di['em']);
        } catch (ProxyCurlException $e) {
            $this->logger->error("Document no proxy: [$url]" . $e->getMessage() . "\n\n");
            throw $e;
        }


        //process 
        try {

            //document
            $startTime = microtime(true);
            $urlWrap = $this->getUrl($index);
            $url = $urlWrap->getHref();
            $this->logger->debug("start process document " . $url . " at " . date('Y-m-d H:i:s'));
            $documentWorker = new Document($url, $curl, $charset);
            $XHTML = $documentWorker->getXHTML();

            $finishTime = microtime(true);
            $this->logger->debug("process document finished at " . date('Y-m-d H:i:s') . ", ended for: " . (string) ($finishTime - $startTime));

            $this->lastProcessedDocument = $XHTML;
            $this->lastProcessedCurl = $curl;
//            ProxyCurl::markReachable($this->di['em'], $curl);

            //containers
            $startTime2 = microtime(true);
            $this->logger->debug("start process containers at " . date('Y-m-d H:i:s'));
            foreach ($this->configXML->document->container as $configXMLContainer) {
                $this->processContainerTag($configXMLContainer); //changes $this->lastProcessedCorteges
            }

            $finishTime = microtime(true);
            $this->logger->info("finish process containers at " . date('Y-m-d H:i:s') . ", ended for: " . (string) ($finishTime - $startTime) . "\n\n");

            $this->corteges[] = $this->lastProcessedCorteges;
        } catch (DocumentProxyException $e) {
            $proxyStr = $curl->getProxy()->getHost() . ":" . $curl->getProxy()->getPort();
            $this->logger->info("Document bad proxy $proxyStr :[$url] \n" . $e->getMessage() . "\n\n");
            ProxyCurl::markUnreachable($this->di['em'], $curl);
            unset($curl);
            return $this->processOne($index);
        } catch (DocumentHttpException $e) {
            $this->logger->warn("Document bad HTTP response: [$url] \n" . $e->getMessage() . " code: [".$e->getCode()."]\n\n");
        } catch (DocumentCurlException $e) {
            $this->logger->error("Document bad curl: [$url] \n" . $e->getMessage() . " code: " . $e->getCode() . "\n\n");
            throw $e;
        } catch (ContainerException $e) {
            $this->logger->error("Container exception: [$url] \n" . $e->getMessage());
            throw $e;
        }

        $curl->close();
        unset($curl);
        return $this->lastProcessedCorteges;
    }

    public function getLastProcessedIndex()
    {
        return $this->lastProcessedIndex;
    }

    public function getLastProcessedDocument()
    {
        return $this->lastProcessedDocument;
    }

    public function getLastProcessedCorteges()
    {
        return $this->lastProcessedCorteges;
    }

    public function getCorteges()
    {
        return $this->corteges;
    }

    public function getUrl($index)
    {
        $pool = $this->getUrlPool();
        return $pool[$index];
    }

    public function getUrlPool()
    {
        return $this->urlPool;
    }

    public function getTotalUrls()
    {
        return count($this->getUrlPool());
    }

    public function getMaxUrlPoolIndex()
    {
        $indexes = array_keys($this->urlPool);
        return max($indexes);
    }

    public function getMinUrlPoolIndex()
    {
        $indexes = array_keys($this->urlPool);
        return min($indexes);
    }

    public function getStart()
    {
        return $this->start;
    }

    public function getFinish()
    {
        return $this->finish;
    }

    /**
     * @todo Продумать, может не отработать при перемешанной (не по порядку) индексации
     * @param int $start
     * @param int $finish
     * @return boolean $result - если удалось установить новый интевал - true, false в случае ошибки    
     */
    public function setInterval($start = null, $finish = null)
    {
        if (null === $start) {
            $start = $this->getMinUrlPoolIndex();
        }
        if (null === $finish) {
            $finish = $this->getMaxUrlPoolIndex();
        }
        if ($start < 0 ||
                $finish < 0 ||
                $finish < $start ||
                $start > $this->getMaxUrlPoolIndex() ||
                $finish > $this->getMaxUrlPoolIndex()) {
            $this->start = 0;
            $this->finish = $this->getMaxUrlPoolIndex();
            $result = false;
        } else {
            $this->start = $start;
            $this->finish = $finish;
            $result = true;
        }
        return $result;
    }

    private function makeUrlPoolFromResource(SimpleXMLElement $configXMLDocument)
    {
        $resource = $configXMLDocument['res'];
        $namespace = 'WsParser\UrlResources';
        $total = 0;
        if (is_callable([$resource, 'getUrls'], true)) {
            $urls = call_user_func([$namespace . '\\' . $resource, 'getUrls'], $configXMLDocument);
            foreach ($urls as $url) {
                $this->urlPool[$url->getPoolIndex()] = $url;
            }
        } else {
            throw new WorkflowException("Resourse class [$resource] not found or doesnt implements UrlResource interface");
        }

        return $total;
    }

    private function makeUrlPoolByTemplate(SimpleXMLElement $configXMLDocument)
    {
        $pageNeedle = '%page%';
        $template = $configXMLDocument['tpl'];
        $start = (int) $configXMLDocument['start'];
        $step = (int) $configXMLDocument['step'];
        $max = (int) $configXMLDocument['max'];
        for ($i = $start; $i <= $max; $i = $i + $step) {
            $url = str_replace($pageNeedle, $i, $template);
            $index = $i;

            $sysLabel = null;
            if (null !== $configXMLDocument['sys_label']) {
                $sysLabel = str_replace($pageNeedle, $i, $configXMLDocument['sys_label']);
            }

            try {
                $urlWrap = new Url($url, $index, $sysLabel);
                $this->urlPool[$index] = $urlWrap;
            } catch (UrlException $e) {
                $message = $e->getMessage();
                $this->logger->warn("Building URL: given url [$url] is not valid url. " . $message);
            }
        }
        $total = count($this->getUrlPool());
        return $total;
    }

    private function processContainerTag(SimpleXMLElement $configXMLContainer)
    {
        if (!isset($configXMLContainer['name'])) {
            throw new WorkflowException("In [$this->configPath] there is a container without name");
        }

        $name = (string) $configXMLContainer['name'];
        $context = $this->getLastProcessedDocument();

        $classname = ( isset($configXMLContainer['worker']) ) ? (string) $configXMLContainer['worker'] : 'WsParser\Containers\DefaultContainer';
        if (!class_exists($classname)) {
            throw new WorkflowException("In [$this->configPath] container [$name] cant call worker");
        }

        $xpath = ( isset($configXMLContainer['xpath']) ? (string) $configXMLContainer['xpath'] : '//body' );
        $containersXML = $context->xpath($xpath);

        if (!isset($this->lastProcessedCorteges[$name])) {
            $this->lastProcessedCorteges[$name] = [];
        }

        //populate containers' config with globals
        $additional = [];
        $additional['currenturl'] = $this->getUrl($this->lastProcessedIndex);
        $additional['curl'] = $this->lastProcessedCurl;
        $additional['em'] = $this->di['em'];

        foreach ($containersXML as $containerXML) {
            $container = new $classname($configXMLContainer, $containerXML, $additional);
            $this->lastProcessedCorteges[$name][] = $container->getCortege();
        }
    }

}
