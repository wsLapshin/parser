#!/usr/bin/env php
<?php
include_once __DIR__ . '/../src/config.php';

if (!$loader = include __DIR__ . '/../vendor/autoload.php') {
    die('You must set up the project dependencies.');
}

Logger::configure(APP_DIR . '/src/log4php.conf.xml');
$app = new \Cilex\Application('Universal wsParser');
$app['em'] = function($c) {
    $doctrineConfig = Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration([ENTITY_DIR], DEV_MODE);
    $entityManager = Doctrine\ORM\EntityManager::create([
        'driver' => MYSQL_DRIVER,
        'user' => MYSQL_USER,
        'password' => MYSQL_PASSWORD,
        'dbname' => MYSQL_DBNAME
    ], $doctrineConfig);
    return $entityManager;
};
$app->command(new \Cilex\Command\GetUrlCommand());
$app->command(new \Cilex\Command\GetProductsCommand());
$app->command(new \Cilex\Command\GetImagesCommand());
$app->command(new \Cilex\Command\Css2xpathCommand());
$app->command(new \Cilex\Command\GreetCommand());
$app->run();
